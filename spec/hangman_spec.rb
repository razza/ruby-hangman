# frozen_string_literal: true

require 'spec_helper'
require_relative '../lib/hangman'

RSpec.describe Hangman do
  it 'stores the word to be retrieved later' do
    word = 'train'
    hangman = Hangman.new word
    expect(hangman.word).to eq word
  end

  it 'creates an empty discovered letter list with the same size as the word' do
    word = 'train'
    hangman = Hangman.new word
    expect(hangman.discovered.size).to eq word.size
    expect(hangman.discovered).to eq Array.new(word.size)
  end

  it 'starts with 6 attempts' do
    hangman = Hangman.new 'train'
    expect(hangman.attempts).to eq 6
  end

  it 'stores the used letters' do
    hangman = Hangman.new 'train'
    expect(hangman.used_letters.eql?(Set[])).to eq true
  end

  describe '#guess' do
    it "fails an attempt when it's wrong" do
      hangman = Hangman.new 'train'
      attempts = hangman.attempts

      result = hangman.guess 'x'
      expect(result).to eq :fail
      expect(hangman.attempts).to eq(attempts - 1)
    end

    it "keeps the attempt count when it's correct" do
      hangman = Hangman.new 'train'
      attempts = hangman.attempts

      result = hangman.guess 't'
      expect(result).to eq :ok
      expect(hangman.attempts).to eq attempts
    end

    it 'the game finishes when all the letters were found' do
      hangman = Hangman.new 'train'
      expect(hangman.guess('t')).to eq :ok
      expect(hangman.guess('r')).to eq :ok
      expect(hangman.guess('a')).to eq :ok
      expect(hangman.guess('i')).to eq :ok
      expect(hangman.guess('n')).to eq :win

      expect(hangman.discovered_word).to eq true
    end

    it 'the game finishes when all the attempts run out' do
      hangman = Hangman.new 'train'
      expect(hangman.guess('b')).to eq :fail
      expect(hangman.guess('c')).to eq :fail
      expect(hangman.guess('d')).to eq :fail
      expect(hangman.guess('e')).to eq :fail
      expect(hangman.guess('f')).to eq :fail
      expect(hangman.guess('g')).to eq :gameover
    end

    it 'the letter is discovered if guessed correctly' do
      hangman = Hangman.new 'train'

      hangman.guess 't'
      expect(hangman.discovered).to eq ['t', nil, nil, nil, nil]

      hangman.guess 'r'
      expect(hangman.discovered).to eq ['t', 'r', nil, nil, nil]

      hangman.guess 'a'
      expect(hangman.discovered).to eq ['t', 'r', 'a', nil, nil]

      hangman.guess 'i'
      expect(hangman.discovered).to eq ['t', 'r', 'a', 'i', nil]

      hangman.guess 'n'
      expect(hangman.discovered).to eq %w[t r a i n]
    end

    it 'stores the used letters when used' do
      hangman = Hangman.new 'train'

      expect(hangman.used_letters.include?('t')).to eq false
      hangman.guess 't'
      expect(hangman.used_letters.include?('t')).to eq true

      expect(hangman.used_letters.include?('t')).to eq true
      hangman.guess 't'
      expect(hangman.used_letters.include?('t')).to eq true

      expect(hangman.used_letters.include?('x')).to eq false
    end

    it 'stores ok letters' do
      hangman = Hangman.new 'train'

      expect(hangman.used_letters.include?('t')).to eq false
      hangman.guess 't'
      expect(hangman.used_letters.include?('t')).to eq true
    end

    it 'stores failed letters' do
      hangman = Hangman.new 'train'

      expect(hangman.used_letters.include?('x')).to eq false
      hangman.guess 'x'
      expect(hangman.used_letters.include?('x')).to eq true
    end

    it "doesn't penalise the player for using letters multiple times" do
      hangman = Hangman.new 'train'

      hangman.guess 'x'
      hangman.guess 'x'
      expect(hangman.attempts).to eq Hangman::ATTEMPT_COUNT - 1
    end
  end
end
