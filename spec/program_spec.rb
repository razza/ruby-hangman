# frozen_string_literal: true

require 'spec_helper'
require_relative '../lib/program'

RSpec.describe Program do
  describe '#valid_input?' do
    it 'allows a single letter' do
      expect(Program.valid_input?('a')).to eq true
    end

    it 'allows a single letter with whitespace' do
      expect(Program.valid_input?("   a    \n\n")).to eq true
    end

    it 'disallows a capital latter' do
      expect(Program.valid_input?('A')).to eq false
    end

    it 'disallows a number' do
      expect(Program.valid_input?('4')).to eq false
    end

    it 'disallows a special character' do
      expect(Program.valid_input?('%')).to eq false
    end

    it 'disallows a foreign letter' do
      expect(Program.valid_input?('%')).to eq false
    end

    it 'disallows multiple letters' do
      expect(Program.valid_input?('train')).to eq false
    end

    it 'disallows multiple digits' do
      expect(Program.valid_input?('42')).to eq false
    end
  end
end
