# frozen_string_literal: true

require_relative 'hangman'

## The main program
class Program
  def initialize(hangman = nil)
    @hangman = hangman || Hangman.new_with_random_word
  end

  def main
    until @hangman.discovered_word
      draw_figure show_word: false

      input = user_input
      puts

      result = @hangman.guess(input)
      gameover if result == :gameover
      win if result == :win
    end
  end

  def user_input
    input = nil
    while input.nil?
      print 'Enter a letter: '
      letter = Program.transform_input(gets)
      if Program.valid_input?(letter)
        input = letter
      else
        puts 'Try typing a single letter this time and press ENTER.'
        print "\033[A\033[A\r\033[K" # cursor up and clear the line
      end
    end

    input
  end

  def draw_figure(show_word: false)
    system 'clear'
    puts @hangman.figure
    puts
    draw_word show_word: show_word
  end

  def draw_word(show_word: true)
    if show_word
      puts @hangman.word.chars.map { |char| " #{char}" }
                   .reduce(:+)
    else
      puts @hangman.discovered.map { |char| char.nil? ? ' _' : " #{char}" }
                   .reduce(:+)
    end

    puts
    if @hangman.used_letters.empty?
      puts
    else
      letters = @hangman.used_letters.map { |char| " #{char}" }
                        .reduce(:+)
      puts "Tried #{letters}"
    end
  end

  def gameover
    draw_figure show_word: true

    puts 'You died. ;-('
    puts
    exit
  end

  def win
    draw_figure show_word: true

    puts 'You win! :-)'
    puts
    exit
  end

  def self.transform_input(input)
    input.downcase.strip
  end

  def self.valid_input?(input)
    input = input.strip
    return false if input.size != 1

    /[a-z]/.match? input
  end
end
